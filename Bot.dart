import 'Hero.dart';

class Bot implements Hero {
  String? name = "Dragon";
  int HP = 10000;
  int Atk = 700;
  int HPHeal = 1000;

  Bot() {
    this.name = name;
    this.HP = HP;
    this.Atk = Atk;
  }

  @override
  attack(int attacked) {
    HP = HP - attacked;
    print("The dragon was attacked with power " + attacked.toString());
    return HP;
  }

  @override
  die() {
    if (HP <= 0) {
      return true;
    } else {
      return false;
    }
  }

  heal() {
    HP = HP + HPHeal;
    return HP;
  }
}
