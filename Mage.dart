import 'Hero.dart';

class Mage implements Hero {
  String? name;
  int HP = 4500;
  int Atk = 1500;
  int atkDamage = 500;

  Mage(String name) {
    this.name = name;
    this.HP = HP;
    this.Atk = Atk;
    this.atkDamage = atkDamage;
  }

  @override
  attack(int attacked) {
    HP = HP - attacked;
    print("The Hero was attacked with power " + attacked.toString());
    return HP;
  }

  @override
  die() {
    if (HP <= 0) {
      return true;
    } else {
      return false;
    }
  }

  damage(int attacked) {
    int damage = atkDamage + attacked;
    return damage;
  }
}
