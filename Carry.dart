import 'Hero.dart';

class Carry implements Hero {
  String? name;
  int HP = 5000;
  int Atk = 1200;
  int atkCritical = 100;

  Carry(String name) {
    this.name = name;
    this.HP = HP;
    this.Atk = Atk;
    this.atkCritical = atkCritical;
  }

  @override
  attack(int attacked) {
    HP = HP - attacked;
    print("The Hero was attacked with power " + attacked.toString());
    return HP;
  }

  @override
  die() {
    if (HP <= 0) {
      return true;
    } else {
      return false;
    }
  }

  critical(int attacked) {
    int critical = atkCritical + attacked;
    return critical;
  }

  weapon(int atkWeapon) {
    Atk = Atk + atkWeapon;
    return Atk;
  }
}
