import 'Hero.dart';

class Fighter implements Hero {
  String? name;
  int HP = 7000;
  int Atk = 1000;
  int atkCombo = 200;

  Fighter(String name) {
    this.name = name;
    this.HP = HP;
    this.Atk = Atk;
    this.atkCombo = atkCombo;
  }

  @override
  attack(int attacked) {
    HP = HP - attacked;
    print("The Hero was attacked with power " + attacked.toString());
    return HP;
  }

  @override
  die() {
    if (HP <= 0) {
      return true;
    } else {
      return false;
    }
  }

  combo(int attacked) {
    int combo = atkCombo + attacked;
    return combo;
  }

  weapon(int atkWeapon) {
    Atk = Atk + atkWeapon;
    return Atk;
  }


}
