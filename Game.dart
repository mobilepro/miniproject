import 'dart:io';

import 'Bot.dart';
import 'Carry.dart';
import 'Fighter.dart';
import 'Gun.dart';
import 'Knife.dart';
import 'Mage.dart';

class GameROV {
  int number = 0;
  int count = 0;
  process() {
    //Select type Hero
    Type();
    int number = int.parse(stdin.readLineSync()!);
    seletType(number);
  }

  Type() {
    print("\nSelect type Hero :");
    print("1. Fighter");
    print("2. Carry");
    print("3. Mage");
  }

  seletType(int number) {
    switch (number) {
      case 1:
        print("\nInput Hero name :");
        String name = stdin.readLineSync()!;
        Fighter fighter = new Fighter(name);

        //Select weapon
        weapon();
        number = int.parse(stdin.readLineSync()!);
        if (number == 1) {
          Knife knift = new Knife();
          fighter.weapon(knift.atkWeapon);
          knift.Pierce();
        }

        //Start game
        Bot bot = new Bot();
        print("\n<<< Start game >>>");

        print(fighter.name.toString() + " VS " + bot.name.toString());

        //showHP
        print("\nStatus :");
        print("HP " + fighter.name.toString() + " = " + fighter.HP.toString());
        print("HP " + bot.name.toString() + " = " + bot.HP.toString());

        while (!fighter.die() && !bot.die()) {
          //Check HP
          if (fighter.die() == true || bot.die() == true) {
            break;
          }

          //show Turn
          showTurnPlayer();

          //Select attack
          skill();
          number = int.parse(stdin.readLineSync()!);
          print("\nYour hero attacks dragon !!!");
          if (number == 1) {
            bot.attack(fighter.Atk);
          } else {
            bot.attack(fighter.combo(fighter.Atk));
          }

          //showHP
          print("\nStatus :");
          print(
              "HP " + fighter.name.toString() + " = " + fighter.HP.toString());
          print("HP " + bot.name.toString() + " = " + bot.HP.toString());

          //Check HP
          if (fighter.die() == true || bot.die() == true) {
            break;
          }

          //show Turn
          showTurnBot();

          //Bot attack
          print("\nDragon attacks your hero !!!");
          fighter.attack(bot.Atk);
          count++;

          //showHP
          print("\nStatus :");
          print(
              "HP " + fighter.name.toString() + " = " + fighter.HP.toString());
          print("HP " + bot.name.toString() + " = " + bot.HP.toString());

          //Check heal
          if (count % 5 == 0) {
            bot.heal();
            print("\nDragon increase HP " + bot.HPHeal.toString());

            //showHP
            print("\nStatus :");
            print("HP " +
                fighter.name.toString() +
                " = " +
                fighter.HP.toString());
            print("HP " + bot.name.toString() + " = " + bot.HP.toString());
          }
        }

        //Check win
        if (fighter.die()) {
          print("\nDragon is win !!!");
        } else {
          print("\n" + fighter.name.toString() + " is win !!!");
        }

        break;

      case 2:
        print("\nInput Hero name :");
        String name = stdin.readLineSync()!;
        Carry carry = new Carry(name);

        //Select weapon
        weapon();
        number = int.parse(stdin.readLineSync()!);
        if (number == 1) {
          Gun gun = new Gun();
          carry.weapon(gun.atkWeapon);
          gun.shoot();
        }

        //Start game
        Bot bot = new Bot();
        print("\n<<< Start game >>>");

        print(carry.name.toString() + " VS " + bot.name.toString());

        //showHP
        print("\nStatus :");
        print("HP " + carry.name.toString() + " = " + carry.HP.toString());
        print("HP " + bot.name.toString() + " = " + bot.HP.toString());

        while (!carry.die() && !bot.die()) {
          //Check HP
          if (carry.die() == true || bot.die() == true) {
            break;
          }

          //show Turn
          showTurnPlayer();

          //Select attack
          skill();
          number = int.parse(stdin.readLineSync()!);
          print("\nYour hero attacks dragon !!!");
          if (number == 1) {
            bot.attack(carry.Atk);
          } else {
            bot.attack(carry.critical(carry.Atk));
          }

          //showHP
          print("\nStatus :");
          print("HP " + carry.name.toString() + " = " + carry.HP.toString());
          print("HP " + bot.name.toString() + " = " + bot.HP.toString());

          //Check HP
          if (carry.die() == true || bot.die() == true) {
            break;
          }

          //show Turn
          showTurnBot();

          //Bot attack
          print("\nDragon attacks your hero !!!");
          carry.attack(bot.Atk);
          count++;

          //showHP
          print("\nStatus :");
          print("HP " + carry.name.toString() + " = " + carry.HP.toString());
          print("HP " + bot.name.toString() + " = " + bot.HP.toString());

          //Check heal
          if (count % 5 == 0) {
            bot.heal();
            print("\nDragon increase HP " + bot.HPHeal.toString());

            //showHP
            print("\nStatus :");
            print("HP " + carry.name.toString() + " = " + carry.HP.toString());
            print("HP " + bot.name.toString() + " = " + bot.HP.toString());
          }
        }

        //Check win
        if (carry.die()) {
          print("\nDragon is win !!!");
        } else {
          print("\n" + carry.name.toString() + " is win !!!");
        }

        break;

      case 3:
        print("\nInput Hero name :");
        String name = stdin.readLineSync()!;
        Mage mage = new Mage(name);

        //Start game
        Bot bot = new Bot();
        print("\n<<< Start game >>>");

        print(mage.name.toString() + " VS " + bot.name.toString());

        //showHP
        print("\nStatus :");
        print("HP " + mage.name.toString() + " = " + mage.HP.toString());
        print("HP " + bot.name.toString() + " = " + bot.HP.toString());

        while (!mage.die() && !bot.die()) {
          //Check HP
          if (mage.die() == true || bot.die() == true) {
            break;
          }

          //show Turn
          showTurnPlayer();

          //Select attack
          skill();
          number = int.parse(stdin.readLineSync()!);
          print("\nYour hero attacks dragon !!!");
          if (number == 1) {
            bot.attack(mage.Atk);
          } else {
            bot.attack(mage.damage(mage.Atk));
          }

          //showHP
          print("\nStatus :");
          print("HP " + mage.name.toString() + " = " + mage.HP.toString());
          print("HP " + bot.name.toString() + " = " + bot.HP.toString());

          //Check HP
          if (mage.die() == true || bot.die() == true) {
            break;
          }

          //show Turn
          showTurnBot();

          //Bot attack
          print("\nDragon attacks your hero !!!");
          mage.attack(bot.Atk);
          count++;

          //showHP
          print("\nStatus :");
          print("HP " + mage.name.toString() + " = " + mage.HP.toString());
          print("HP " + bot.name.toString() + " = " + bot.HP.toString());

          //Check heal
          if (count % 5 == 0) {
            bot.heal();
            print("\nDragon increase HP " + bot.HPHeal.toString());

            //showHP
            print("\nStatus :");
            print("HP " + mage.name.toString() + " = " + mage.HP.toString());
            print("HP " + bot.name.toString() + " = " + bot.HP.toString());
          }
        }

        //Check win
        if (mage.die()) {
          print("\nDragon is win !!!");
        } else {
          print("\n" + mage.name.toString() + " is win !!!");
        }

        break;
      default:
    }
  }

  weapon() {
    print("\nDo you want to use weapon?");
    print("1. Yes");
    print("2. No");
  }

  skill() {
    print("\nSelect attack :");
    print("1. Skill normal");
    print("2. Skill ultimate");
  }

  showTurnPlayer() {
    print("");
    print("");
    print("------------Your Turn------------");
  }

  showTurnBot() {
    print("");
    print("");
    print("------------Bot Turn------------");
  }
}

void main(List<String> args) {
  print("Welcome to game ROV");
  GameROV game = new GameROV();
  game.process();
}
