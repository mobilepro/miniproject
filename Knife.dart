import 'Weapon.dart';

class Knife extends Weapon with Piercable {
  String? name = "Sapata";
  int atkWeapon = 100;

  Knife() {
    this.name = name;
    this.atkWeapon = atkWeapon;
  }

  @override
  Pierce() {
    print("Your weapon is $name");
    print("You have atk +$atkWeapon");
  }
}

class Piercable {
  Pierce() {}
}
