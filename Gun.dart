import 'Weapon.dart';

class Gun extends Weapon with shootable{
  String? name = "MP5";
  int atkWeapon = 150;

  Gun() {
    this.name = name;
    this.atkWeapon=atkWeapon;
  }
  
  @override
  shoot() {
    print("Your weapon is $name");
    print("You have atk +$atkWeapon");
  }
}

class shootable {
  shoot() {}
}
